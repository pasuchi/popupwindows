package com.example.popupmenuproyect.menuoption

import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.PopupWindow
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.popupmenuproyect.menuoption.model.MenuOption
import com.example.popupmenuproyect.menuoption.adapter.MenuOptionAdapter
import com.example.popupmenuproyect.R
import kotlinx.android.synthetic.main.fragment_dialog.*
import kotlinx.android.synthetic.main.layout_menu.view.*

class DialogFragmentCustom : DialogFragment() {
    val listModelData: MutableList<MenuOption> = mutableListOf()
    lateinit var menu: PopupWindow
    lateinit var imageViewFromParent: ImageView
    lateinit var recyclerView: RecyclerView
    lateinit var menuOptionAdapter: MenuOptionAdapter
    lateinit var menuView: View
    var with :Int = 0
    var height : Int = 0


    var drawable :Int =
        R.drawable.ic_gps_not_fixed_white_24dp


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.fullScreen)
    }

    companion object
    fun  getInstance(with: Int, height: Int,parentFragmentManager: FragmentManager): DialogFragmentCustom {
        val dialogFragment =
            DialogFragmentCustom()
        val bundle = Bundle()
        bundle.putInt("with",with)
        bundle.putInt("height",height)
        dialogFragment.arguments = bundle
        parentFragmentManager.beginTransaction().add(dialogFragment,"DIALOG").commit()
        return  dialogFragment
    }


    init {
        listModelData.add(
            MenuOption(
                0,
                "Text1"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text2"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text3"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text4"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text5"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text6"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text7"
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        container.setOnClickListener {
            menu.dismiss()
            parentFragmentManager.beginTransaction().remove(this).commit()
        }
        menuView = layoutInflater.inflate(R.layout.layout_menu, null)
        recyclerView = menuView.rvListMenu
        menuOptionAdapter =
            MenuOptionAdapter(
                listModelData
            )
        recyclerView.adapter = menuOptionAdapter

        getArgunmentsDialog()
        addView(with,height)

    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = WindowManager.LayoutParams.MATCH_PARENT
        params.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams

    }

    fun getArgunmentsDialog(){
        with = arguments?.getInt("with")?:0
        height = arguments?.getInt("height")?:0

    }



    fun addView(with: Int, height: Int) {
        imageViewFromParent = ImageView(context)
        imageViewFromParent.layoutParams = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.WRAP_CONTENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        imageViewFromParent.y = height.toFloat()
        imageViewFromParent.x = with.toFloat()
        imageViewFromParent.setImageDrawable(
            ContextCompat.getDrawable(
                context!!,
                drawable
            )
        )
        container.addView(imageViewFromParent)
        imageViewFromParent.setOnClickListener {
            menu = PopupWindow(
                menuView,
                with,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )
            menu.showAtLocation(imageViewFromParent, Gravity.NO_GRAVITY, 0, height)
        }
        imageViewFromParent.postDelayed(object : Runnable {
            override fun run() {
                imageViewFromParent.callOnClick()
                imageViewFromParent.isEnabled = false
            }
        }, 1)

    }


    fun setDrawableResourceId(drawable: Int) {
        this.drawable = drawable
    }




}