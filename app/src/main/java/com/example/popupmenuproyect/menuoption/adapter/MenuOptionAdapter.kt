package com.example.popupmenuproyect.menuoption.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.popupmenuproyect.menuoption.model.MenuOption
import com.example.popupmenuproyect.databinding.MenuOptionItemsBinding

class MenuOptionAdapter constructor(val listModelData: MutableList<MenuOption>) :
    RecyclerView.Adapter<MenuOptionAdapter.CustomAdapter>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapter {
        return CustomAdapter(
            MenuOptionItemsBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun getItemCount(): Int {
        return listModelData.size
    }

    override fun onBindViewHolder(holder: CustomAdapter, position: Int) {
        holder.bind(listModelData[position])
    }

    class CustomAdapter(val view: MenuOptionItemsBinding) : RecyclerView.ViewHolder(view.root) {
        fun bind(menuOption: MenuOption) {
            view.modelData = menuOption
        }
    }

}