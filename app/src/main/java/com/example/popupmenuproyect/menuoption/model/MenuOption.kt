package com.example.popupmenuproyect.menuoption.model

class MenuOption(
    var imageResource: Int,
    var title: String
)