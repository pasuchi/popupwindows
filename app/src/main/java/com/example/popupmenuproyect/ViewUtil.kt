package com.example.popupmenuproyect

import android.content.res.Resources
import android.widget.ImageView

class ViewUtil {

    companion object{

        fun getDimensions(view : ImageView, resources: Resources):IntArray{

            val locations = IntArray(2)
            view.getLocationOnScreen(locations)
            val y = locations[1]
            locations[1] = y - statusBarHeight(resources)

            return locations
        }
        private fun statusBarHeight(res: Resources): Int {
            return (24 * res.displayMetrics.density).toInt()
        }

    }


}