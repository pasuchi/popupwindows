package com.example.popupmenuproyect

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.fragment.app.Fragment
import com.example.popupmenuproyect.menuoption.DialogFragmentCustom
import kotlinx.android.synthetic.main.fragment_blank.*

/**
 * A simple [Fragment] subclass.
 */
class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_blank, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivHamburger.setOnClickListener {
            val locations =  ViewUtil.getDimensions(ivHamburger,resources)

            DialogFragmentCustom().getInstance(
                locations[0],
                locations[1],
                parentFragmentManager
            )

        }


    }

    fun getOption() {
        ivHamburger.getViewTreeObserver().addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                ivHamburger.getViewTreeObserver().removeOnGlobalLayoutListener(this)
                val locations = IntArray(2)
                ivHamburger.getLocationOnScreen(locations)
                val x = locations[0]
                val y = locations[1]
                val n = ivHamburger.height
                val height = y - n

            }
        })
        ivHamburger.requestLayout()
    }

    fun getHeigthWithOutToolsBar(container: ViewGroup, height: Int): Int {
        val global = container
        val dm = DisplayMetrics()
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(dm)
        val topOffset: Int = dm.heightPixels - global.getMeasuredHeight()
        return height - topOffset
    }

}
