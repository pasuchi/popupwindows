package com.example.popupmenuproyect

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.PopupWindow
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.popupmenuproyect.menuoption.adapter.MenuOptionAdapter
import com.example.popupmenuproyect.menuoption.model.MenuOption
import kotlinx.android.synthetic.main.fragment_dialog.*
import kotlinx.android.synthetic.main.layout_menu.view.*


class MenuOptionFragment : Fragment() {
    lateinit var menu: PopupWindow
    lateinit var imageViewFromParent: ImageView
    lateinit var recyclerView: RecyclerView
    lateinit var menuOptionAdapter: MenuOptionAdapter
    lateinit var menuView: View
    var drawable :Int = R.drawable.ic_gps_not_fixed_white_24dp
    val listModelData: MutableList<MenuOption> = mutableListOf()

    init {
        listModelData.add(
            MenuOption(
                0,
                "Text1"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text2"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text3"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text4"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text5"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text6"
            )
        )
        listModelData.add(
            MenuOption(
                0,
                "Text7"
            )
        )
    }
    companion object
    fun  getInstance(container: ViewGroup, button: ImageView,parentFragmentManager: FragmentManager):MenuOptionFragment{
        val dialogFragment = MenuOptionFragment()
        dialogFragment.setImagePosition(container,button)
        parentFragmentManager.beginTransaction().add(R.id.nav_host_fragment,dialogFragment).commit()
        return  dialogFragment
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dialog, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        container.setOnClickListener {
            menu.dismiss()
            parentFragmentManager.beginTransaction().remove(this).commit()
        }
        menuView = layoutInflater.inflate(R.layout.layout_menu, null)
        recyclerView = menuView.rvListMenu
        menuOptionAdapter =
            MenuOptionAdapter(
                listModelData
            )
        recyclerView.adapter = menuOptionAdapter
    }


    fun setImagePosition(container: ViewGroup, button: ImageView) {
        button.getViewTreeObserver().addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                button.getViewTreeObserver().removeOnGlobalLayoutListener(this)
                val locations = IntArray(2)
                button.getLocationOnScreen(locations)
                val x = locations[0]
                val y = locations[1]



                addView(x, y,getHeigthWithOutToolsBar(container,y))

            }
        })
    }


    fun addView(with: Int, height: Int,heightWithNoToolbar: Int) {
        imageViewFromParent = ImageView(context)
        imageViewFromParent.layoutParams = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.WRAP_CONTENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        imageViewFromParent.y = heightWithNoToolbar.toFloat()
        imageViewFromParent.x = with.toFloat()
        imageViewFromParent.setImageDrawable(
            ContextCompat.getDrawable(
                context!!,
                drawable
            )
        )
        container.addView(imageViewFromParent)
        imageViewFromParent.setOnClickListener {
            menu = PopupWindow(
                menuView,
                with,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )
            menu.showAtLocation(imageViewFromParent, Gravity.NO_GRAVITY, 0, height)
        }
        imageViewFromParent.postDelayed(object : Runnable {
            override fun run() {
                imageViewFromParent.callOnClick()
                imageViewFromParent.isEnabled = false
            }
        }, 1)

    }


    fun setDrawableResourceId(drawable: Int) {
        this.drawable = drawable
    }

    fun getHeigthWithOutToolsBar(container:ViewGroup,height: Int):Int {
        val global = container
        val dm = DisplayMetrics()
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(dm)
        val topOffset: Int = dm.heightPixels - global.getMeasuredHeight()
        return height - topOffset
    }

}
